<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="./css/style.css">
<title>スロットゲーム</title>
</head>
<body>
	<table class="game_title">
		<tr>
			<td><img src="./slot_images/title.png"></td>
		</tr>
	</table>
	<hr>
	<%String  buttonName = "スロットを回す!";
		int[] arrayNumber = new int[3];

		for(int i = 0; i < 3; i++){
			arrayNumber[i] = (int)(Math.random()*9)+1;
		}
	%>


	<table class="main_slot">
		<tr>
			<td><img src="./slot_images/<%=arrayNumber[0] %>.png"></td>
			<td><img src="./slot_images/<%=arrayNumber[1] %>.png"></td>
			<td><img src="./slot_images/<%=arrayNumber[2] %>.png"></td>
		</tr>
		<tr>
			<td colspan="3" class="main_button">
				<form name="sample" action="slot.jsp" method="post">
				<input type="submit" name="submit_btn" value=<%=buttonName %>>
				</form>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="main_result">
				<% if(arrayNumber[0] == arrayNumber[1] && arrayNumber[0] == arrayNumber[2]) {%>
				<img src="./slot_images/hit.png">
				<%} else {%>
				<img src="./slot_images/nohit.png">
				<%} %>
			</td>
		</tr>
	</table>
</body>
</html>